{:ok, proc} = WebSocketSyncApi.open("ws://localhost:8756/ws", [])
try do
	IO.puts("#{inspect proc, pretty: true}")
	for x <- 1..2 do
		WebSocketSyncApi.write(proc, {:text, "test \##{x}"})
		r = WebSocketSyncApi.read(proc)
		IO.puts("#{inspect r, pretty: true}")
	end
after
	r = WebSocketSyncApi.close(proc, wait: :infinity)
	# r = WebSocketSyncApi.close(proc, code: 1011, wait: :infinity)
	IO.puts("closed, reason=#{inspect r, pretty: true}")
end
