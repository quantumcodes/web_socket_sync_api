import Config

config :logger, :console,
	level: :debug,
	format: "==== $level $time $metadata====\n$message\n\n",
	metadata: [:file, :line, :mfa, :pid]
