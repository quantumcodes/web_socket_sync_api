defmodule WebSocketSyncApi do
  @moduledoc """
  Documentation for `WebSocketSyncApi`.
  """
  use WebSockex
  require Logger

  defmacro log(m, level \\ :debug) do
    quote do
      Logger.log(unquote(level), inspect(unquote(m), pretty: true))
    end
  end

  @spec open(binary | WebSockex.Conn.t(), [
          {:async
           | :cacerts
           | :debug
           | :extra_headers
           | :handle_initial_conn_failure
           | :insecure
           | :name
           | :socket_connect_timeout
           | :socket_recv_timeout
           | :ssl_options, atom | list | non_neg_integer | {:global, any} | {:via, atom, any}}
        ]) :: {:error, any} | {:ok, pid}
  def open(url, opts \\ []) do
    state = %{
      caller: self(),
      frames: [],
      receivers: []
    }

    WebSockex.start_link(url, __MODULE__, state, opts)
  end

  @spec write(atom | pid | port | reference | {atom, atom}, any) :: any
  def write(client, frame) do
    log({:write, client, frame})
    WebSockex.cast(client, {:send, frame})
  end

  @spec read(atom | pid | port | reference | {atom, atom}, nil | maybe_improper_list | map) :: any
  def read(client, opts \\ []) do
    log({:read, client, opts})
    timeout = opts[:timeout] || :infinity
    WebSockex.cast(client, :receive)

    receive do
      r ->
        r
    after
      timeout ->
        {:error, {:timeout, timeout}}
    end
  end

  @spec close(atom | pid | port | reference | {atom, atom}, nil | maybe_improper_list | map) ::
          any
  def close(client, opts \\ []) do
    code = opts[:code]
    reason = opts[:reason] || ""
    wait = opts[:wait]

    frame =
      if code do
        WebSockex.cast(client, {:close, {code, reason}})
      else
        WebSockex.cast(client, :close)
      end

    write(client, frame)

    if wait do
      ref = Process.monitor(client)

      receive do
        {:DOWN, ^ref, _, _, reason} ->
          reason
      after
        wait ->
          :timeout
      end
    end
  end

  defp receive_frame(state) do
    case state[:frames] do
      [] ->
        proc =
          spawn(fn ->
            receive do
              r ->
                send(state[:caller], r)
            end
          end)

        nstate = %{state | receivers: state[:receivers] ++ [proc]}
        log({:receive, nstate})
        {:ok, nstate}

      [f | fs] ->
        send(state[:caller], f)
        nstate = %{state | frames: fs}
        log({:receive, nstate})
        {:ok, nstate}
    end
  end

  def handle_cast({:send, frame}, state) do
    log({{:send, frame}, state})
    {:reply, frame, state}
  end

  def handle_cast(:receive, state) do
    log({:receive, state})
    receive_frame(state)
  end

  def handle_cast(:close, state) do
    log({:close, state})
    {:close, state}
  end

  def handle_cast({:close, frame}, state) do
    log({:close, state})
    {:close, frame, state}
  end

  def handle_frame(frame, state) do
    log({:received, frame, state})

    case state[:receivers] do
      [] ->
        {:ok, %{state | frames: state[:frames] ++ [frame]}}

      [r | rs] ->
        send(r, frame)
        {:ok, %{state | receivers: rs}}
    end
  end
end
